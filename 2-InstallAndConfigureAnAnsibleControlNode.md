# <a name='InstallandconfigureanAnsiblecontrolnode'></a>Install and configure an Ansible control node

<!-- vscode-markdown-toc -->

* [Install required packages](#Installrequiredpackages)
* [Create a static host inventory file](#Createastatichostinventoryfile)
* [Create a configuration file](#Createaconfigurationfile)

<!-- vscode-markdown-toc-config
	numbering=false
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

## <a name='Installrequiredpackages'></a>Install required packages

## <a name='Createastatichostinventoryfile'></a>Create a static host inventory file

## <a name='Createaconfigurationfile'></a>Create a configuration file
