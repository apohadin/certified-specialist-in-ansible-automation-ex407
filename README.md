# Red Hat Certified Specialist in Ansible Automation (EX407)

## Links

* [Red Hat Certified Specialist in Ansible Automation](https://www.redhat.com/en/services/certification/rhcs-ansible-automation)
* [Red Hat Certified Specialist in Ansible Automation exam ](https://www.redhat.com/en/services/training/ex407-red-hat-certified-specialist-in-ansible-automation-exam)

## Person of Interest

## Table of Content

* [Understand core components of Ansible](1-UnderstandCoreComponentsOfAnsible.md)
* [Install and configure an Ansible control node](2-InstallAndConfigureAnAnsibleControlNode.md)
* [Configure Ansible managed nodes](3-ConfigureAnsibleManagedNodes.md)
* [Create simple shell scripts that run ad hoc Ansible commands](4-CreateSimpleShellScriptsThatRunAdhocAnsibleCommands.md)
* [Use both static and dynamic inventories to define groups of hosts](5-UseBothStaticAndDynamicInventoriesToDefineGroupsOfHosts.md)
* [Utilize an existing dynamic inventory script](6-UtilizeAnExistingDynamicInventoryScript.md)
* [Create Ansible plays and playbooks](7-CreateAnsiblePlaysAndPlaybooks.md)
* [Use Ansible modules for system administration tasks that work with:](8-UseAnsibleModulesForSystemAdministrationTasks.md)
* [Create and use templates to create customized configuration files](9-CreateAndUseTemplatesToCreateCustomizedConfigurationFiles.md)
* [Work with Ansible variables and facts](10-WorkWithAnsibleVariablesAndFacts.md)
* [Create and work with roles](11-CreateAndWorkWithRoles.md)
* [Download roles from an Ansible Galaxy and use them](12-DownloadRolesFromAnAnsibleGalaxyAndUseThem.md)
* [Manage parallelism](13-ManageParallelism.md)
* [Use Ansible Vault in playbooks to protect sensitive data](14-UseAnsibleVaultInPlaybooksToProtectSensitiveData.md)
* [Use provided documentation to look up specific information about Ansible modules and commands](15-UseProvidedDocumentationToLookUpSpecificInformationAboutAnsibleModulesAndCommands.md)
