# <a name='UnderstandcorecomponentsofAnsible'></a>Understand core components of Ansible

<!-- vscode-markdown-toc -->

* [Understand core components of Ansible](#UnderstandcorecomponentsofAnsible)
	* [Inventories](#Inventories)
	* [Modules](#Modules)
	* [Variables](#Variables)
	* [Facts](#Facts)
	* [Plays](#Plays)
	* [Playbooks](#Playbooks)
	* [Configuration files](#Configurationfiles)

<!-- vscode-markdown-toc-config
	numbering=false
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

## <a name='Inventories'></a>Inventories

## <a name='Modules'></a>Modules

## <a name='Variables'></a>Variables

## <a name='Facts'></a>Facts

## <a name='Plays'></a>Plays

## <a name='Playbooks'></a>Playbooks

## <a name='Configurationfiles'></a>Configuration files
